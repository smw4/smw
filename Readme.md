# Super Mario World

A repository for the launch script and compiled binaries which is part of the AUR packages

https://aur.archlinux.org/packages/supermarioworld
https://aur.archlinux.org/packages/smw-bin

The repositories,compiled binaries and packages are not associated with the [smw](https://github.com/snesrev/smw) project or Nintendo and all rights go to Nintendo.

Please support the smw project as this project wouldn't exist without it.